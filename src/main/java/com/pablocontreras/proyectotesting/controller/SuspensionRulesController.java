package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.SuspensionRules;
import com.pablocontreras.proyectotesting.service.SuspensionRulesDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class SuspensionRulesController {
    @Autowired
    @Qualifier("servicio_suspensionrules")
    private SuspensionRulesDaoImpl suspensionRulesDao;

    @PutMapping("/suspensionrule")
    public void createSuspensionRules(@RequestBody @Valid SuspensionRules suspensionRules) {
        suspensionRulesDao.create(suspensionRules);
    }

    @PostMapping("/suspensionrule")
    public void updateSuspensionRules(@RequestBody @Valid SuspensionRules suspensionRules) {
        suspensionRulesDao.update(suspensionRules);
    }

    @DeleteMapping("/suspensionrule/{id}")
    public void deleteSuspensionRules(@PathVariable("id") int id) {
        suspensionRulesDao.delete(id);
    }

    @GetMapping("/suspensionrule/{id}")
    public SuspensionRules getSuspensionRules(@PathVariable("id") int id){
        return suspensionRulesDao.get(id);
    }

    @GetMapping("/suspensionrule")
    public List<SuspensionRules> getSuspensionRules() {
        return suspensionRulesDao.getAll();
    }

    @GetMapping("/suspensionrule/exists/{id}")
    public boolean existSuspensionRules(@PathVariable("id") int id){
        return suspensionRulesDao.exist(id);
    }
}

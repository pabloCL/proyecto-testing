package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.Subject;
import com.pablocontreras.proyectotesting.service.SubjectDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class SubjectController {
    @Autowired
    @Qualifier("servicio_subject")
    private SubjectDaoImpl subjectDao;
    @PutMapping("/subject")
    public void createSubject(@RequestBody @Valid Subject subject) {
        subjectDao.create(subject);
    }

    @PostMapping("/subject")
    public void updateSubject(@RequestBody @Valid Subject subject) {
        subjectDao.update(subject);
    }

    @DeleteMapping("/subject/{id}")
    public void deleteSubject(@PathVariable("id") int id) {
        subjectDao.delete(id);
    }

    @GetMapping("/subject/{id}")
    public Subject getSubject(@PathVariable("id") int id){
        return subjectDao.get(id);
    }

    @GetMapping("/subject")
    public List<Subject> getSubject() {
        return subjectDao.getAll();
    }

    @GetMapping("/subject/exists/{id}")
    public boolean existSubject(@PathVariable("id") int id){
        return subjectDao.exist(id);
    }
}

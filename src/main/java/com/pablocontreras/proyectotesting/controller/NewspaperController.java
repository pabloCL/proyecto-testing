package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.Newspaper;
import com.pablocontreras.proyectotesting.service.NewspaperDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class NewspaperController {
    @Autowired
    @Qualifier("servicio_newspaper")
    private NewspaperDaoImpl newspaperDao;
    @PutMapping("/newspaper")
    public void createNewspaper(@RequestBody @Valid Newspaper newspaper) {
        newspaperDao.create(newspaper);
    }

    @PostMapping("/newspaper")
    public void updateNewspaper(@RequestBody @Valid Newspaper newspaper) {
        newspaperDao.update(newspaper);
    }

    @DeleteMapping("/newspaper/{id}")
    public void deleteNewspaper(@PathVariable("id") int id) {
        newspaperDao.delete(id);
    }

    @GetMapping("/newspaper/{id}")
    public Newspaper getNewspaper(@PathVariable("id") int id){
        return newspaperDao.get(id);
    }

    @GetMapping("/newspaper")
    public List<Newspaper> getNewspapers() {
        return newspaperDao.getAll();
    }

    @GetMapping("/newspaper/exists/{id}")
    public boolean existNewspaper(@PathVariable("id") int id){
        return newspaperDao.exist(id);
    }
}

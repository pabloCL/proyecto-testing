package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.Multimedia;
import com.pablocontreras.proyectotesting.service.MultimediaDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class MultimediaController {
    @Autowired
    @Qualifier("servicio_multimedia")
    private MultimediaDaoImpl multimediaDao;
    @PutMapping("/multimedia")
    public void createMultimedia(@RequestBody @Valid Multimedia multimedia) {
        multimediaDao.create(multimedia);
    }

    @PostMapping("/multimedia")
    public void updateMultimedia(@RequestBody @Valid Multimedia multimedia) {
        multimediaDao.update(multimedia);
    }

    @DeleteMapping("/multimedia/{id}")
    public void deleteMultimedia(@PathVariable("id") int id) {
        multimediaDao.delete(id);
    }

    @GetMapping("/multimedia/{id}")
    public Multimedia getMultimedia(@PathVariable("id") int id){
        return multimediaDao.get(id);
    }

    @GetMapping("/multimedia")
    public List<Multimedia> getMultimedia() {
        return multimediaDao.getAll();
    }

    @GetMapping("/multimedia/exists/{id}")
    public boolean existMultimedia(@PathVariable("id") int id){
        return multimediaDao.exist(id);
    }
}

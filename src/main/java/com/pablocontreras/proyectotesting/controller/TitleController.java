package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.Title;
import com.pablocontreras.proyectotesting.service.TitleDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class TitleController {
    @Autowired
    @Qualifier("servicio_title")
    private TitleDaoImpl titleDao;
    @PutMapping("/title")
    public void createTitle(@RequestBody @Valid Title title) {
        titleDao.create(title);
    }

    @PostMapping("/title")
    public void updateTitle(@RequestBody @Valid Title title) {
        titleDao.update(title);
    }

    @DeleteMapping("/title/{id}")
    public void deleteTitle(@PathVariable("id") int id) {
        titleDao.delete(id);
    }

    @GetMapping("/title/{id}")
    public Title getTitle(@PathVariable("id") int id){
        return titleDao.get(id);
    }

    @GetMapping("/title")
    public List<Title> getTitle() {
        return titleDao.getAll();
    }

    @GetMapping("/title/exists/{id}")
    public boolean existTitle(@PathVariable("id") int id){
        return titleDao.exist(id);
    }
}

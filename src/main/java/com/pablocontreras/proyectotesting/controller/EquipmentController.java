package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.Equipment;
import com.pablocontreras.proyectotesting.service.EquipmentDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class EquipmentController {
    @Autowired
    @Qualifier("servicio_equipment")
    private EquipmentDaoImpl equipmentDao;
    @PutMapping("/equipment")
    public void createEquipment(@RequestBody @Valid Equipment equipment) {
        equipmentDao.create(equipment);
    }

    @PostMapping("/equipment")
    public void updateEquipment(@RequestBody @Valid Equipment equipment) {
        equipmentDao.update(equipment);
    }

    @DeleteMapping("/equipment/{id}")
    public void deleteEquipment(@PathVariable("id") int id) {
        equipmentDao.delete(id);
    }

    @GetMapping("/equipment/{id}")
    public Equipment getEquipment(@PathVariable("id") int id){
        return equipmentDao.get(id);
    }

    @GetMapping("/equipment")
    public List<Equipment> getEquipment() {
        return equipmentDao.getAll();
    }

    @GetMapping("/equipment/exists/{id}")
    public boolean existEquipment(@PathVariable("id") int id){
        return equipmentDao.exist(id);
    }
}

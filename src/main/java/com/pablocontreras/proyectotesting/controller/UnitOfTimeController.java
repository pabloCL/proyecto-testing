package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.UnitOfTime;
import com.pablocontreras.proyectotesting.service.UnitOfTimeDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class UnitOfTimeController {
    @Autowired
    @Qualifier("servicio_unitoftime")
    private UnitOfTimeDaoImpl unitOfTimeDao;

    @PutMapping("/unitoftime")
    public void createUnitOfTime(@RequestBody @Valid UnitOfTime unitOfTime) {
        unitOfTimeDao.create(unitOfTime);
    }

    @PostMapping("/unitoftime")
    public void updateUnitOfTime(@RequestBody @Valid UnitOfTime unitOfTime) {
        unitOfTimeDao.update(unitOfTime);
    }

    @DeleteMapping("/unitoftime/{id}")
    public void deleteUnitOfTime(@PathVariable("id") int id) {
        unitOfTimeDao.delete(id);
    }

    @GetMapping("/unitoftime/{id}")
    public UnitOfTime getUnitOfTime(@PathVariable("id") int id){
        return unitOfTimeDao.get(id);
    }

    @GetMapping("/unitoftime")
    public List<UnitOfTime> getUnitOfTimes() {
        return unitOfTimeDao.getAll();
    }

    @GetMapping("/unitoftime/exists/{id}")
    public boolean existUnitOfTime(@PathVariable("id") int id){
        return unitOfTimeDao.exist(id);
    }
}

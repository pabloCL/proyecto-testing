package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.LoanCondition;
import com.pablocontreras.proyectotesting.service.LoanConditionDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class LoanConditionController {
    @Autowired
    @Qualifier("servicio_loancondition")
    private LoanConditionDaoImpl loanConditionDao;

    @PutMapping("/loancondition")
    public void createLoanCondition(@RequestBody @Valid LoanCondition loanCondition) {
        loanConditionDao.create(loanCondition);
    }

    @PostMapping("/loancondition")
    public void updateLoanCondition(@RequestBody @Valid LoanCondition loanCondition) {
        loanConditionDao.update(loanCondition);
    }

    @DeleteMapping("/loancondition/{id}")
    public void deleteLoanCondition(@PathVariable("id") int id) {
        loanConditionDao.delete(id);
    }

    @GetMapping("/loancondition/{id}")
    public LoanCondition getLoanCondition(@PathVariable("id") int id){
        return loanConditionDao.get(id);
    }

    @GetMapping("/loancondition")
    public List<LoanCondition> getLoanConditions() {
        return loanConditionDao.getAll();
    }

    @GetMapping("/loancondition/exists/{id}")
    public boolean existLoanCondition(@PathVariable("id") int id){
        return loanConditionDao.exist(id);
    }
}

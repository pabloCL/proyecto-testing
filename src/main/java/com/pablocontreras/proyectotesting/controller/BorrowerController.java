package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.BCategoryBorrower;
import com.pablocontreras.proyectotesting.model.Borrower;
import com.pablocontreras.proyectotesting.service.BorrowerDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/biblioteca")
public class BorrowerController {
    @Autowired
    @Qualifier("servicio_borrower")
    private BorrowerDaoImpl borrowerDao;

    @PostMapping(value = "/borrower")
    public Borrower createBorrower(@RequestBody @Valid Borrower borrower) {
        borrowerDao.create(borrower);
        return borrower;
    }

    @PutMapping("/borrower")
    public Borrower updateBorrower(@RequestBody @Valid Borrower borrower) {
        borrowerDao.update(borrower);
        return borrower;
    }

    @DeleteMapping("/borrower/{rut}")
    public void deleteBorrower(@PathVariable("rut") int rut) {
        borrowerDao.delete(rut);
    }

    @GetMapping("/borrower/{rut}")
    public Borrower getBorrower(@PathVariable("rut") int rut){
        return borrowerDao.get(rut);
    }

    @GetMapping("/borrower")
    public List<Borrower> getBorrowers() {
        return borrowerDao.getAll();
    }

    @GetMapping("/borrower/exists/{rut}")
    public boolean existBorrower(@PathVariable("rut") int rut){
        return borrowerDao.exist(rut);
    }

    @GetMapping("/borrower/count")
    public long countBorrower(){
        return borrowerDao.count();
    }

    @GetMapping("/borrower/detail")
    public List<BCategoryBorrower> borrowerDetail(){
        return borrowerDao.borrowersDetail();
    }
}

package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.TitleCategory;
import com.pablocontreras.proyectotesting.service.TitleCategoryDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class TitleCategoryController {
    @Autowired
    @Qualifier("servicio_titlecategory")
    private TitleCategoryDaoImpl titleCategoryDao;
    @PutMapping("/titlecategory")
    public void createTitleCategory(@RequestBody @Valid TitleCategory titleCategory) {
        titleCategoryDao.create(titleCategory);
    }

    @PostMapping("/titlecategory")
    public void updateTitleCategory(@RequestBody @Valid TitleCategory titleCategory) {
        titleCategoryDao.update(titleCategory);
    }

    @DeleteMapping("/titlecategory/{id}")
    public void deleteTitleCategory(@PathVariable("id") int id) {
        titleCategoryDao.delete(id);
    }

    @GetMapping("/titlecategory/{id}")
    public TitleCategory getTitleCategory(@PathVariable("id") int id){
        return titleCategoryDao.get(id);
    }

    @GetMapping("/titlecategory")
    public List<TitleCategory> getTitleCategory() {
        return titleCategoryDao.getAll();
    }

    @GetMapping("/titlecategory/exists/{id}")
    public boolean existTitleCategory(@PathVariable("id") int id){
        return titleCategoryDao.exist(id);
    }
}

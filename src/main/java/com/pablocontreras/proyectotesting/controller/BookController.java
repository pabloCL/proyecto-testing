package com.pablocontreras.proyectotesting.controller;


import com.pablocontreras.proyectotesting.model.Book;
import com.pablocontreras.proyectotesting.service.BookDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class BookController {
    @Autowired
    @Qualifier("servicio_book")
    private BookDaoImpl bookDao;

    @PutMapping("/book")
    public void createBook(@RequestBody @Valid Book book) {
        bookDao.create(book);
    }

    @PostMapping("/book")
    public void updateBook(@RequestBody @Valid Book book) {
        bookDao.update(book);
    }

    @DeleteMapping("/book/{isbn}")
    public void deleteBook(@PathVariable("isbn") String isbn) {
        bookDao.delete(isbn);
    }

    @GetMapping("/book/{isbn}")
    public Book getBook(@PathVariable("isbn") String isbn){
        return bookDao.get(isbn);
    }

    @GetMapping("/book")
    public List<Book> getBook() {
        return bookDao.getAll();
    }

    @GetMapping("/book/exists/{isbn}")
    public boolean existBook(@PathVariable("isbn") String isbn){
        return bookDao.exist(isbn);
    }
}

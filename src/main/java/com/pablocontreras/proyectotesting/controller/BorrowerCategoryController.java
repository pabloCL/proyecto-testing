package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.BorrowerCategory;
import com.pablocontreras.proyectotesting.service.BorrowerCategoryDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class BorrowerCategoryController {
    @Autowired
    @Qualifier("servicio_bcategory")
    private BorrowerCategoryDaoImpl borrowerDao;
    @PutMapping("/borrowercategory")
    public void createBorrower(@RequestBody @Valid BorrowerCategory borrowerCategory) {
        borrowerDao.create(borrowerCategory);
    }

    @PostMapping("/borrowercategory")
    public void updateBorrower(@RequestBody @Valid BorrowerCategory borrowerCategory) {
        borrowerDao.update(borrowerCategory);
    }

    @DeleteMapping("/borrowercategory/{id}")
    public void deleteBorrower(@PathVariable("id") int id) {
        borrowerDao.delete(id);
    }

    @GetMapping("/borrowercategory/{id}")
    public BorrowerCategory getBorrower(@PathVariable("id") int id){
        return borrowerDao.get(id);
    }

    @GetMapping("/borrowercategory")
    public List<BorrowerCategory> getBorrowers() {
        return borrowerDao.getAll();
    }

    @GetMapping("/borrowercategory/exists/{id}")
    public boolean existBorrower(@PathVariable("id") int id){
        return borrowerDao.exist(id);
    }
}

package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.Copy;
import com.pablocontreras.proyectotesting.service.CopyDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class CopyController {
    @Autowired
    @Qualifier("servicio_copy")
    private CopyDaoImpl copyDao;
    @PutMapping("/copy")
    public void createCopy(@RequestBody @Valid Copy copy) {
        copyDao.create(copy);
    }

    @PostMapping("/copy")
    public void updateCopy(@RequestBody @Valid Copy copy) {
        copyDao.update(copy);
    }

    @DeleteMapping("/copy/{id}")
    public void deleteCopy(@PathVariable("id") int id) {
        copyDao.delete(id);
    }

    @GetMapping("/copy/{id}")
    public Copy getCopy(@PathVariable("id") int id){
        return copyDao.get(id);
    }

    @GetMapping("/copy")
    public List<Copy> getCopy() {
        return copyDao.getAll();
    }

    @GetMapping("/copy/exists/{id}")
    public boolean existCopy(@PathVariable("id") int id){
        return copyDao.exist(id);
    }
}

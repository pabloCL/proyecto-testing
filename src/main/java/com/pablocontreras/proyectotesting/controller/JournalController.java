package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.Journal;
import com.pablocontreras.proyectotesting.service.JournalDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class JournalController {
    @Autowired
    @Qualifier("servicio_journal")
    private JournalDaoImpl journalDao;

    @PutMapping("/journal")
    public void createJournal(@RequestBody @Valid Journal journal) {
        journalDao.create(journal);
    }

    @PostMapping("/journal")
    public void updateJournal(@RequestBody @Valid Journal journal) {
        journalDao.update(journal);
    }

    @DeleteMapping("/journal/{id}")
    public void deleteJournal(@PathVariable("id") int id) {
        journalDao.delete(id);
    }

    @GetMapping("/journal/{id}")
    public Journal getJournal(@PathVariable("id") int id){
        return journalDao.get(id);
    }

    @GetMapping("/journal")
    public List<Journal> getJournals() {
        return journalDao.getAll();
    }

    @GetMapping("/journal/exists/{id}")
    public boolean existJournal(@PathVariable("id") int id){
        return journalDao.exist(id);
    }

}

package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.Suspension;
import com.pablocontreras.proyectotesting.service.SuspensionDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/biblioteca")
public class SuspensionController {
    @Autowired
    @Qualifier("servicio_suspension")
    private SuspensionDaoImpl suspensionDao;

    @PutMapping("/suspension")
    public void createSuspension(@RequestBody @Valid Suspension suspension) {
        suspensionDao.create(suspension);
    }

    @PostMapping("/suspension")
    public void updateSuspension(@RequestBody @Valid Suspension suspension) {
        suspensionDao.update(suspension);
    }

    @DeleteMapping("/suspension/{id}")
    public void deleteSuspension(@PathVariable("id") int id) {
        suspensionDao.delete(id);
    }

    @GetMapping("/suspension/{id}")
    public Suspension getSuspension(@PathVariable("id") int id){
        return suspensionDao.get(id);
    }

    @GetMapping("/suspension")
    public List<Suspension> getSuspensions() {
        return suspensionDao.getAll();
    }

    @GetMapping("/suspension/exists/{id}")
    public boolean existSuspension(@PathVariable("id") int id){
        return suspensionDao.exist(id);
    }

}
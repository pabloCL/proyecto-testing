package com.pablocontreras.proyectotesting.controller;

import com.pablocontreras.proyectotesting.model.Loan;
import com.pablocontreras.proyectotesting.service.LoanDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class LoanController {
    @Autowired
    @Qualifier("servicio_loan")
    private LoanDaoImpl loanDao;

    @PutMapping("/loan")
    public void createLoan(@RequestBody @Valid Loan loan) {
        loanDao.create(loan);
    }

    @PostMapping("/loan")
    public void updateLoan(@RequestBody @Valid Loan loan) {
        loanDao.update(loan);
    }

    @DeleteMapping("/loan/{id}")
    public void deleteLoan(@PathVariable("id") int id) {
        loanDao.delete(id);
    }

    @GetMapping("/loan/{id}")
    public Loan getLoan(@PathVariable("id") int id){
        return loanDao.get(id);
    }

    @GetMapping("/loan")
    public List<Loan> getLoans() {
        return loanDao.getAll();
    }

    @GetMapping("/loan/exists/{id}")
    public boolean existLoan(@PathVariable("id") int id){
        return loanDao.exist(id);
    }

}

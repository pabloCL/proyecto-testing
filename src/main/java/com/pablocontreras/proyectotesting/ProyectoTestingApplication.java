package com.pablocontreras.proyectotesting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoTestingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProyectoTestingApplication.class, args);
    }
}

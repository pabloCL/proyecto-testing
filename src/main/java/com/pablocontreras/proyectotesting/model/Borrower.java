package com.pablocontreras.proyectotesting.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "borrower")
public class Borrower implements Serializable {
    @Id
    @Column(name = "borrower_rut")
    private int rut;
    @Column(name="borrower_name")
    private String borrower_name;
    @Column(name="borrower_cellphone")
    private String cellphone;
    @Column(name = "borrower_email")
    private String email;
    @Column(name = "borrowercategory_identifier")
    private int bcid;

    @OneToMany(mappedBy = "borrowerSuspension",cascade = CascadeType.ALL)
    private List<Suspension> suspensionList = new ArrayList<>();

    @OneToMany(mappedBy = "borrowerLoan",cascade = CascadeType.ALL)
    private List<Loan> loanList = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "borrowercategory_identifier",referencedColumnName ="borrowercategory_identifier",insertable = false,updatable = false)
    private BorrowerCategory borrowerCategory;

    public Borrower() {
    }

    public Borrower(int rut, String borrower_name, String cellphone, String email, int bcid) {
        this.rut = rut;
        this.borrower_name = borrower_name;
        this.cellphone = cellphone;
        this.email = email;
        this.bcid = bcid;
    }

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBorrower_name() {
        return borrower_name;
    }

    public void setBorrower_name(String borrower_name) {
        this.borrower_name = borrower_name;
    }

    public int getBcid() {
        return bcid;
    }

    public void setBcid(int bcid) {
        this.bcid = bcid;
    }

    @Override
    public String toString() {
        return "Borrower{" +
                "rut=" + rut +
                ", borrower_name='" + borrower_name + '\'' +
                ", cellphone='" + cellphone + '\'' +
                ", email='" + email + '\'' +
                ", bcid=" + bcid +
                '}';
    }
}

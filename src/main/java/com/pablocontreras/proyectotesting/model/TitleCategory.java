package com.pablocontreras.proyectotesting.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "titlecategory")
public class TitleCategory implements Serializable {
    @GeneratedValue
    @Id
    @Column(name = "titlecategory_identifier")
    private int id;
    @Column(name="titlecategory_name")
    private String name;

    @OneToMany(mappedBy = "titleTitleCategory",cascade = CascadeType.ALL)
    private List<Title> titleList = new ArrayList<>();

    @OneToMany(mappedBy = "titleCategoryLoan",cascade = CascadeType.ALL)
    private List<LoanCondition> loanConditions = new ArrayList<>();

    public TitleCategory() {
    }

    public TitleCategory(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TitleCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

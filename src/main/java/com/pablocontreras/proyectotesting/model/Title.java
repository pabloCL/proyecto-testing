package com.pablocontreras.proyectotesting.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "title")
public class Title implements Serializable {
    @GeneratedValue
    @Id
    @Column(name = "title_identifier")
    private int id;
    @Column(name="title_name")
    private String name;
    @Column(name="title_year")
    private String year;
    @Column(name = "book_identifier")
    private Integer bookid;
    @Column(name = "journal_identifier")
    private Integer journalid;
    @Column(name = "multimedia_identifier")
    private Integer multimediaid;
    @Column(name = "newspaper_identifier")
    private Integer newspaperid;
    @Column(name = "equipment_identifier")
    private Integer equipmentid;
    @Column(name="title_replacementcost")
    private String replacementcost;
    @Column(name = "subject_identifier")
    private int subject;
    @Column(name = "titlecategory_identifier")
    private int tcategory;

    @OneToMany(mappedBy = "titleCopy",cascade = CascadeType.ALL)
    private List<Copy> copyList = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subject_identifier", referencedColumnName = "subject_identifier", insertable = false, updatable = false)
    private Subject subjectTitle;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "titlecategory_identifier", referencedColumnName = "titlecategory_identifier", insertable = false, updatable = false)
    private TitleCategory titleTitleCategory;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "book_identifier", referencedColumnName = "book_identifier", insertable = false, updatable = false)
    private Book bookTitle;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "journal_identifier", referencedColumnName = "journal_identifier", insertable = false, updatable = false)
    private Journal journalTitle;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "multimedia_identifier", referencedColumnName = "multimedia_identifier", insertable = false, updatable = false)
    private Multimedia multimediaTitle;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "newspaper_identifier", referencedColumnName = "newspaper_identifier", insertable = false, updatable = false)
    private Newspaper newspaperTitle;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "equipment_identifier", referencedColumnName = "equipment_identifier", insertable = false, updatable = false)
    private Equipment equipmentTitle;

    public Title() {
    }

    public Title(String name, String year, Integer bookid, Integer journalid, Integer multimediaid, Integer newspaperid, Integer equipmentid, String replacementcost, int subject, int tcategory) {
        this.name = name;
        this.year = year;
        this.bookid = bookid;
        this.journalid = journalid;
        this.multimediaid = multimediaid;
        this.newspaperid = newspaperid;
        this.equipmentid = equipmentid;
        this.replacementcost = replacementcost;
        this.subject = subject;
        this.tcategory = tcategory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getReplacementcost() {
        return replacementcost;
    }

    public void setReplacementcost(String replacementcost) {
        this.replacementcost = replacementcost;
    }

    public Integer getBookid() {
        return bookid;
    }

    public void setBookid(Integer bookid) {
        this.bookid = bookid;
    }

    public Integer getJournalid() {
        return journalid;
    }

    public void setJournalid(Integer journalid) {
        this.journalid = journalid;
    }

    public Integer getMultimediaid() {
        return multimediaid;
    }

    public void setMultimediaid(Integer multimediaid) {
        this.multimediaid = multimediaid;
    }

    public Integer getNewspaperid() {
        return newspaperid;
    }

    public void setNewspaperid(Integer newspaperid) {
        this.newspaperid = newspaperid;
    }

    public Integer getEquipmentid() {
        return equipmentid;
    }

    public void setEquipmentid(Integer equipmentid) {
        this.equipmentid = equipmentid;
    }

    public int getSubject() {
        return subject;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }

    public int getTcategory() {
        return tcategory;
    }

    public void setTcategory(int tcategory) {
        this.tcategory = tcategory;
    }
}

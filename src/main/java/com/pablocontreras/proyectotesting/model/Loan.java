package com.pablocontreras.proyectotesting.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "loan")
public class Loan implements Serializable {
    @GeneratedValue
    @Id
    @Column(name = "loan_identifier")
    private int loanid;
    @Column(name = "loan_startdate")
    @Temporal(TemporalType.DATE)
    private Date startdate;
    @Column(name="loan_duedate")
    @Temporal(TemporalType.DATE)
    private Date duedate;
    @Column(name="loan_returndate")
    @Temporal(TemporalType.DATE)
    private Date returndate;
    @Column(name = "borrower_rut")
    private int borrowerrut;
    @Column(name = "copy_identifier")
    private int copyid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "borrower_rut",referencedColumnName = "borrower_rut",insertable = false,updatable = false)
    private Borrower borrowerLoan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "copy_identifier", referencedColumnName = "copy_identifier", insertable = false, updatable = false)
    private Copy copyLoan;

    public Loan() {
    }

    public Loan(Date startdate, Date duedate, Date returndate, int borrowerrut, int copyid) {
        this.startdate = startdate;
        this.duedate = duedate;
        this.returndate = returndate;
        this.borrowerrut = borrowerrut;
        this.copyid = copyid;
    }

    public int getLoanid() {
        return loanid;
    }

    public void setLoanid(int loanid) {
        this.loanid = loanid;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getDuedate() {
        return duedate;
    }

    public void setDuedate(Date duedate) {
        this.duedate = duedate;
    }

    public Date getReturndate() {
        return returndate;
    }

    public void setReturndate(Date returndate) {
        this.returndate = returndate;
    }

    public int getBorrowerrut() {
        return borrowerrut;
    }

    public void setBorrowerrut(int borrowerrut) {
        this.borrowerrut = borrowerrut;
    }

    public int getCopyid() {
        return copyid;
    }

    public void setCopyid(int copyid) {
        this.copyid = copyid;
    }

    @Override
    public String toString() {
        return "Loan{" +
                "loanid=" + loanid +
                ", startdate=" + startdate +
                ", duedate=" + duedate +
                ", returndate=" + returndate +
                ", borrowerrut='" + borrowerrut + '\'' +
                ", copyid='" + copyid + '\'' +
                ", borrowerLoan=" + borrowerLoan +
                ", copyLoan=" + copyLoan +
                '}';
    }
}

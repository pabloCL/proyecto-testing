package com.pablocontreras.proyectotesting.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "journal")
public class Journal implements Serializable {
    @GeneratedValue
    @Id
    @Column(name = "journal_identifier")
    private int id;
    @Column(name="journal_volumen")
    private int volumen;

    @OneToOne(mappedBy = "journalTitle", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private Title titleJournal;

    public Journal() {
    }

    public Journal(int volumen) {
        this.volumen = volumen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVolumen() {
        return volumen;
    }

    public void setVolumen(int volumen) {
        this.volumen = volumen;
    }

    @Override
    public String toString() {
        return "Journal{" +
                "id=" + id +
                ", volumen=" + volumen +
                '}';
    }
}

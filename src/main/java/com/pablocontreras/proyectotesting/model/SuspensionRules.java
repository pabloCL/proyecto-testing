package com.pablocontreras.proyectotesting.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "suspensionrules")
public class SuspensionRules implements Serializable {
    @GeneratedValue
    @Id
    @Column(name = "suspensionrule_identifier")
    private int id;
    @Column(name = "suspensionrule_delayunitoftime")
    private int delayunit;
    @Column(name = "suspensionrule_suspensionunitoftime")
    private int suspensionunit;
    @Column(name = "unitoftime_identifier")
    private int unitoftime;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unitoftime_identifier", referencedColumnName = "unitoftime_identifier", insertable = false, updatable = false)
    private UnitOfTime unitSuspensionrule;

    public SuspensionRules() {
    }

    public SuspensionRules(int delayunit, int suspensionunit, int unitoftime) {
        this.delayunit = delayunit;
        this.suspensionunit = suspensionunit;
        this.unitoftime = unitoftime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDelayunit() {
        return delayunit;
    }

    public void setDelayunit(int delayunit) {
        this.delayunit = delayunit;
    }

    public int getSuspensionunit() {
        return suspensionunit;
    }

    public void setSuspensionunit(int suspensionunit) {
        this.suspensionunit = suspensionunit;
    }

    public int getUnitoftime() {
        return unitoftime;
    }

    public void setUnitoftime(int unitoftime) {
        this.unitoftime = unitoftime;
    }

    @Override
    public String toString() {
        return "SuspensionRules{" +
                "id=" + id +
                ", delayunit=" + delayunit +
                ", suspensionunit=" + suspensionunit +
                ", unitoftime=" + unitoftime +
                '}';
    }
}

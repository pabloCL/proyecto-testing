package com.pablocontreras.proyectotesting.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "unitoftime")
public class UnitOfTime implements Serializable{
    @GeneratedValue
    @Id
    @Column(name = "unitoftime_identifier")
    private int utid;
    @Column(name="unitoftime_name")
    private String utname;

    @OneToMany(mappedBy = "unitSuspension",cascade = CascadeType.ALL)
    private List<Suspension> suspensionList = new ArrayList<>();

    @OneToMany(mappedBy = "unitSuspensionrule",cascade = CascadeType.ALL)
    private List<SuspensionRules> suspensionRules = new ArrayList<>();

    public UnitOfTime() {
    }

    public UnitOfTime(String utname) {
        this.utname = utname;
    }

    public int getUtid() {
        return utid;
    }

    public void setUtid(int utid) {
        this.utid = utid;
    }

    public String getUtname() {
        return utname;
    }

    public void setUtname(String utname) {
        this.utname = utname;
    }

    @Override
    public String toString() {
        return "UnitOfTime{" +
                "utid=" + utid +
                ", utname='" + utname + '\'' +
                '}';
    }
}

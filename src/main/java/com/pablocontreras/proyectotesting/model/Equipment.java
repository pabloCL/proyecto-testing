package com.pablocontreras.proyectotesting.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "equipment")
public class Equipment implements Serializable {
    @GeneratedValue
    @Id
    @Column(name = "equipment_identifier")
    private int id;
    @Column(name = "equipment_description")
    private String description;
    @OneToOne(mappedBy = "equipmentTitle", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private Title titleEquipment;
    public Equipment() {
    }

    public Equipment(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Equipment{" +
                "id=" + id +
                ", description='" + description + '\'' +
                '}';
    }
}

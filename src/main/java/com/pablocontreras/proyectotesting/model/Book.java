package com.pablocontreras.proyectotesting.model;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "book")
public class Book implements Serializable {
    @GeneratedValue
    @Id
    @Column(name = "book_identifier")
    private int id;
    @Column(name="book_author")
    private String autor;
    @Column(name="book_edition")
    private String edicion;
    @Column(name = "book_isbn")
    private String isbn;
    @Column(name = "book_editorial")
    private String editorial;

    @OneToOne(mappedBy = "bookTitle", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private Title titleBook;

    public Book() {
    }

    public Book(int id, String autor, String edicion, String isbn, String editorial) {
        this.id = id;
        this.autor = autor;
        this.edicion = edicion;
        this.isbn = isbn;
        this.editorial = editorial;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEdicion() {
        return edicion;
    }

    public void setEdicion(String edicion) {
        this.edicion = edicion;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", autor='" + autor + '\'' +
                ", edicion='" + edicion + '\'' +
                ", isbn='" + isbn + '\'' +
                ", editorial='" + editorial + '\'' +
                '}';
    }
}

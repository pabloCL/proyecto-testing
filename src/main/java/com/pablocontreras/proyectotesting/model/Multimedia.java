package com.pablocontreras.proyectotesting.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "multimedia")
public class Multimedia implements Serializable {
    @GeneratedValue
    @Id
    @Column(name = "multimedia_identifier")
    private int id;
    @Column(name = "multimedia_platform")
    private String platform;

    @OneToOne(mappedBy = "multimediaTitle", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private Title titleMultimedia;

    public Multimedia() {
    }

    public Multimedia(String platform) {
        this.platform = platform;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @Override
    public String toString() {
        return "Multimedia{" +
                "id=" + id +
                ", platform='" + platform + '\'' +
                '}';
    }
}

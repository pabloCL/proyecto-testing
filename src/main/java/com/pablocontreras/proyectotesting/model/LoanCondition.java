package com.pablocontreras.proyectotesting.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "loancondition")
public class LoanCondition  implements Serializable {
    @Id
    @Column(name = "loancondition_identifier")
    private int id;
    @Column(name="loancondition_maxnumberofunitoftime")
    private int maxnumberofunitoftime;
    @Column(name="loancondition_maxnumberofrenewals")
    private int maxnumberofrenewals;
    @Column(name = "loancondition_unitoftime")
    private int unitoftime;
    @Column(name = "loancondition_fee")
    private int fee;
    @Column(name = "borrowercategory_identifier")
    private int bcid;
    @Column(name = "titlecategory_identifier")
    private int tcid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "borrowercategory_identifier", referencedColumnName = "borrowercategory_identifier", insertable = false, updatable = false)
    private BorrowerCategory borrowerLoan;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "titlecategory_identifier", referencedColumnName = "titlecategory_identifier", insertable = false, updatable = false)
    private TitleCategory titleCategoryLoan;

    public LoanCondition() {
    }

    public LoanCondition(int id, int maxnumberofunitoftime, int maxnumberofrenewals, int unitoftime, int fee, int bcid, int tcid) {
        this.id = id;
        this.maxnumberofunitoftime = maxnumberofunitoftime;
        this.maxnumberofrenewals = maxnumberofrenewals;
        this.unitoftime = unitoftime;
        this.fee = fee;
        this.bcid = bcid;
        this.tcid = tcid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMaxnumberofunitoftime() {
        return maxnumberofunitoftime;
    }

    public void setMaxnumberofunitoftime(int maxnumberofunitoftime) {
        this.maxnumberofunitoftime = maxnumberofunitoftime;
    }

    public int getMaxnumberofrenewals() {
        return maxnumberofrenewals;
    }

    public void setMaxnumberofrenewals(int maxnumberofrenewals) {
        this.maxnumberofrenewals = maxnumberofrenewals;
    }

    public int getUnitoftime() {
        return unitoftime;
    }

    public void setUnitoftime(int unitoftime) {
        this.unitoftime = unitoftime;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public int getBcid() {
        return bcid;
    }

    public void setBcid(int bcid) {
        this.bcid = bcid;
    }

    public int getTcid() {
        return tcid;
    }

    public void setTcid(int tcid) {
        this.tcid = tcid;
    }

    @Override
    public String toString() {
        return "LoanCondition{" +
                "id=" + id +
                ", maxnumberofunitoftime=" + maxnumberofunitoftime +
                ", maxnumberofrenewals=" + maxnumberofrenewals +
                ", unitoftime=" + unitoftime +
                ", fee=" + fee +
                ", bcid=" + bcid +
                ", tcid=" + tcid +
                '}';
    }
}

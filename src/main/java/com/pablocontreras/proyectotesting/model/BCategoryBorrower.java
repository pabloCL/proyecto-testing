package com.pablocontreras.proyectotesting.model;

public class BCategoryBorrower {

    private int rut;
    private String name;
    private String phone;
    private String email;
    private String categoria;

    public BCategoryBorrower() {
    }

    public BCategoryBorrower(int rut, String name, String phone, String email, String categoria) {
        this.rut = rut;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.categoria = categoria;
    }

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}

package com.pablocontreras.proyectotesting.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "borrowercategory")
public class BorrowerCategory implements Serializable {
    @GeneratedValue
    @Id
    @Column(name = "borrowercategory_identifier")
    private int id;
    @Column(name="borrowercategory_name")
    private String name;
    @Column(name="borrowercategory_maxnumberofloans")
    private String maxLoans;

    @OneToMany(mappedBy = "borrowerCategory",cascade = CascadeType.ALL)
    private List<Borrower> borrowerList = new ArrayList<>();

    @OneToMany(mappedBy = "borrowerLoan",cascade = CascadeType.ALL)
    private List<LoanCondition> loanConditions = new ArrayList<>();

    public BorrowerCategory() {
    }

    public BorrowerCategory(String name, String maxLoans) {
        this.name = name;
        this.maxLoans = maxLoans;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaxLoans() {
        return maxLoans;
    }

    public void setMaxLoans(String maxLoans) {
        this.maxLoans = maxLoans;
    }

    @Override
    public String toString() {
        return "BorrowerCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", maxLoans='" + maxLoans + '\'' +
                '}';
    }
}

package com.pablocontreras.proyectotesting.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "copy")
public class Copy implements Serializable{
    @GeneratedValue
    @Id
    @Column(name = "copy_identifier")
    private int id;
    @Column(name="copy_acquisitiondate")
    private String acquisitiondate;
    @Column(name="title_identifier")
    private String titleid;

    @OneToMany(mappedBy = "copyLoan",cascade = CascadeType.ALL)
    private List<Loan> loanList = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "title_identifier", referencedColumnName = "title_identifier", insertable = false, updatable = false)
    private Title titleCopy;

    public Copy() {
    }

    public Copy(String acquisitiondate, String titleid) {
        this.acquisitiondate = acquisitiondate;
        this.titleid = titleid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAcquisitiondate() {
        return acquisitiondate;
    }

    public void setAcquisitiondate(String acquisitiondate) {
        this.acquisitiondate = acquisitiondate;
    }

    public String getTitleid() {
        return titleid;
    }

    public void setTitleid(String titleid) {
        this.titleid = titleid;
    }

    @Override
    public String toString() {
        return "Copy{" +
                "id=" + id +
                ", acquisitiondate='" + acquisitiondate + '\'' +
                ", titleid='" + titleid + '\'' +
                '}';
    }
}

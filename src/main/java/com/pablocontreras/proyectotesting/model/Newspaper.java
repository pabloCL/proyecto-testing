package com.pablocontreras.proyectotesting.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "newspaper")
public class Newspaper implements Serializable {
    @GeneratedValue
    @Id
    @Column(name = "newspaper_identifier")
    private int id;
    @Column(name="newspaper_day")
    private int day;
    @Column(name="newspaper_month")
    private int month;

    @OneToOne(mappedBy = "newspaperTitle", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private Title titleNewspaper;

    public Newspaper() {
    }

    public Newspaper(int day, int month) {
        this.day = day;
        this.month = month;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    @Override
    public String toString() {
        return "Newspaper{" +
                "id=" + id +
                ", day=" + day +
                ", month=" + month +
                '}';
    }
}

package com.pablocontreras.proyectotesting.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "subject")
public class Subject implements Serializable {
    @GeneratedValue
    @Id
    @Column(name = "subject_identifier")
    private int id;
    @Column(name="subject_name")
    private String name;

    @OneToMany(mappedBy = "subjectTitle",cascade = CascadeType.ALL)
    private List<Title> titleList = new ArrayList<>();
    
    public Subject() {
    }

    public Subject(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

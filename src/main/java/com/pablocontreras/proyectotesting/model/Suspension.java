package com.pablocontreras.proyectotesting.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "suspension")
public class Suspension implements Serializable {
    @GeneratedValue
    @Id
    @Column(name = "suspension_identifier")
    private int id;
    @Column(name = "suspension_description")
    private String description;
    @Temporal(TemporalType.DATE)
    @Column(name = "suspension_startdate")
    private Date startdate;
    @Column(name = "suspension_numberofunitoftime")
    private int numberofunitoftime;
    @Column(name = "unitoftime_identifier")
    private int unitoftime;
    @Column(name="borrower_rut")
    private int rut;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "borrower_rut",referencedColumnName = "borrower_rut",insertable = false,updatable = false)
    private Borrower borrowerSuspension;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unitoftime_identifier",referencedColumnName = "unitoftime_identifier",insertable = false,updatable = false)
    private UnitOfTime unitSuspension;

    public Suspension() {
    }

    public Suspension(String description, Date startdate, int numberofunitoftime, int unitoftime, int rut) {
        this.description = description;
        this.startdate = startdate;
        this.numberofunitoftime = numberofunitoftime;
        this.unitoftime = unitoftime;
        this.rut = rut;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public int getNumberofunitoftime() {
        return numberofunitoftime;
    }

    public void setNumberofunitoftime(int numberofunitoftime) {
        this.numberofunitoftime = numberofunitoftime;
    }

    public int getUnitoftime() {
        return unitoftime;
    }

    public void setUnitoftime(int unitoftime) {
        this.unitoftime = unitoftime;
    }

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    @Override
    public String toString() {
        return "Suspension{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", startdate=" + startdate +
                ", numberofunitoftime=" + numberofunitoftime +
                ", unitoftime='" + unitoftime + '\'' +
                ", rut=" + rut +
                '}';
    }
}

package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.BorrowerCategoryDAO;
import com.pablocontreras.proyectotesting.model.BorrowerCategory;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_bcategory")
public class BorrowerCategoryDaoImpl {
    @Autowired
    @Qualifier("repositorio_bcategory")
    private BorrowerCategoryDAO borrowerCategoryDAO;
    public void create(BorrowerCategory borrowerCategory) throws CreateException {
        borrowerCategoryDAO.save(borrowerCategory);
    }

    public BorrowerCategory update(BorrowerCategory borrowerCategory) throws UpdateException {
        return borrowerCategoryDAO.save(borrowerCategory);
    }

    public BorrowerCategory delete(int id) throws DeleteException {
        BorrowerCategory borrowerCategory = borrowerCategoryDAO.findById(id);
        borrowerCategoryDAO.delete(borrowerCategory);
        return borrowerCategory;
    }

    public BorrowerCategory get(int id){
        return borrowerCategoryDAO.findById(id);
    }

    public List<BorrowerCategory> getAll(){
        return borrowerCategoryDAO.findAll();
    }

    public boolean exist(int id){
        return borrowerCategoryDAO.existsById(id);
    }
}

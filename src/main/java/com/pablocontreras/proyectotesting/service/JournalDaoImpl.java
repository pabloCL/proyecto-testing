package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.JournalDAO;
import com.pablocontreras.proyectotesting.model.Journal;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_journal")
public class JournalDaoImpl {
    @Autowired
    @Qualifier("repositorio_journal")
    private JournalDAO journalDAO;

    public void create(Journal journal) throws CreateException {
        journalDAO.save(journal);
    }

    public Journal update(Journal journal) throws UpdateException {
        return journalDAO.save(journal);
    }

    public Journal delete(int id) throws DeleteException {
        Journal journal = journalDAO.findById(id);
        journalDAO.delete(journal);
        return journal;
    }

    public Journal get(int id){
        return journalDAO.findById(id);
    }

    public List<Journal> getAll(){
        return journalDAO.findAll();
    }

    public boolean exist(int id){
        return journalDAO.existsById(id);
    }
}

package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.CopyDAO;
import com.pablocontreras.proyectotesting.model.Copy;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_copy")
public class CopyDaoImpl {
    @Autowired
    @Qualifier("repositorio_copy")
    private CopyDAO copyDAO;
    public void create(Copy copy) throws CreateException {
        copyDAO.save(copy);
    }

    public Copy update(Copy loan) throws UpdateException {
        return copyDAO.save(loan);
    }

    public Copy delete(int id) throws DeleteException {
        Copy borrower = copyDAO.findById(id);
        copyDAO.delete(borrower);
        return borrower;
    }

    public Copy get(int id){
        return copyDAO.findById(id);
    }

    public List<Copy> getAll(){
        return copyDAO.findAll();
    }

    public boolean exist(int id){
        return copyDAO.existsById(id);
    }
}

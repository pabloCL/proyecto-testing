package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.NewspaperDAO;
import com.pablocontreras.proyectotesting.model.Newspaper;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_newspaper")
public class NewspaperDaoImpl {
    @Autowired
    @Qualifier("repositorio_newspaper")
    private NewspaperDAO newspaperDAO;

    public void create(Newspaper newspaper) throws CreateException {
        newspaperDAO.save(newspaper);
    }

    public Newspaper update(Newspaper newspaper) throws UpdateException {
        return newspaperDAO.save(newspaper);
    }

    public Newspaper delete(int id) throws DeleteException {
        Newspaper newspaper = newspaperDAO.findById(id);
        newspaperDAO.delete(newspaper);
        return newspaper;
    }

    public Newspaper get(int id){
        return newspaperDAO.findById(id);
    }

    public List<Newspaper> getAll(){
        return newspaperDAO.findAll();
    }

    public boolean exist(int id){
        return newspaperDAO.existsById(id);
    }
}

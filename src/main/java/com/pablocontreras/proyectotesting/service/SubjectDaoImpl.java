package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.SubjectDAO;
import com.pablocontreras.proyectotesting.model.Subject;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_subject")
public class SubjectDaoImpl {
    @Autowired
    @Qualifier("repositorio_subject")
    private SubjectDAO subjectDAO;

    public void create(Subject subject) throws CreateException {
        subjectDAO.save(subject);
    }

    public Subject update(Subject subject) throws UpdateException {
        return subjectDAO.save(subject);
    }

    public Subject delete(int id) throws DeleteException {
        Subject subject = subjectDAO.findById(id);
        subjectDAO.delete(subject);
        return subject;
    }

    public Subject get(int id){
        return subjectDAO.findById(id);
    }

    public List<Subject> getAll(){
        return subjectDAO.findAll();
    }

    public boolean exist(int id){
        return subjectDAO.existsById(id);
    }
}

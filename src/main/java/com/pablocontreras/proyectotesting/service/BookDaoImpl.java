package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.BookDAO;
import com.pablocontreras.proyectotesting.model.Book;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_book")
public class BookDaoImpl {
    @Autowired
    @Qualifier("repositorio_book")
    private BookDAO bookDAO;

    public void create(Book book) throws CreateException {
        bookDAO.save(book);
    }

    public Book update(Book book) throws UpdateException {
        return bookDAO.save(book);
    }

    public Book delete(String nombre) throws DeleteException {
        Book book = bookDAO.findByAutor(nombre);
        bookDAO.delete(book);
        return book;
    }

    public Book get(String isbn){
        return bookDAO.findByIsbn(isbn);
    }

    public List<Book> getAll(){
        return bookDAO.findAll();
    }

    public boolean exist(String isbn){
        return bookDAO.existsByIsbn(isbn);
    }

}


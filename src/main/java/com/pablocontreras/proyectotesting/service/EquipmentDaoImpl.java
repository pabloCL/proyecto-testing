package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.EquipmentDAO;
import com.pablocontreras.proyectotesting.model.Equipment;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_equipment")
public class EquipmentDaoImpl {
    @Autowired
    @Qualifier("repositorio_equipment")
    private EquipmentDAO equipmentDAO;

    public void create(Equipment equipment) throws CreateException {
        equipmentDAO.save(equipment);
    }

    public Equipment update(Equipment equipment) throws UpdateException {
        return equipmentDAO.save(equipment);
    }

    public Equipment delete(int id) throws DeleteException {
        Equipment equipment = equipmentDAO.findById(id);
        equipmentDAO.delete(equipment);
        return equipment;
    }

    public Equipment get(int id){
        return equipmentDAO.findById(id);
    }

    public List<Equipment> getAll(){
        return equipmentDAO.findAll();
    }

    public boolean exist(int id){
        return equipmentDAO.existsById(id);
    }
}

package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.BorrowerDAO;
import com.pablocontreras.proyectotesting.model.BCategoryBorrower;
import com.pablocontreras.proyectotesting.model.Borrower;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("servicio_borrower")
public class BorrowerDaoImpl {
    @Autowired
    @Qualifier("repositorio_borrower")
    private BorrowerDAO borrowerDAO;

    public Borrower create(Borrower borrower) throws CreateException {
        borrowerDAO.save(borrower);
        return borrower;
    }

    public Borrower update(Borrower borrower) throws UpdateException {
        return borrowerDAO.save(borrower);
    }

    public Borrower delete(int rut) throws DeleteException {
        Borrower borrower = borrowerDAO.findByRut(rut);
        borrowerDAO.delete(borrower);
        return borrower;
    }

    public Borrower get(int rut){
        return borrowerDAO.findByRut(rut);
    }

    public List<Borrower> getAll(){
        return borrowerDAO.findAll();
    }

    public boolean exist(int rut){
        return borrowerDAO.existsByRut(rut);
    }

    public long count() {
        return borrowerDAO.count();
    }

    public List<BCategoryBorrower> borrowersDetail(){
        List<BCategoryBorrower> borrowers = new ArrayList<>();
        List<Object[]> lista = borrowerDAO.borrowersDetail();
        for (Object[] datos:lista){
            borrowers.add(new BCategoryBorrower((int)datos[0],(String)datos[1],(String)datos[2],(String)datos[3],(String)datos[4]));
        }
        return borrowers;
    }

}

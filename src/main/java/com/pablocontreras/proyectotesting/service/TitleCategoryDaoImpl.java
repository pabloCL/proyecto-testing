package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.TitleCategoryDAO;
import com.pablocontreras.proyectotesting.model.TitleCategory;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_titlecategory")
public class TitleCategoryDaoImpl {
    @Autowired
    @Qualifier("repositorio_titlecategory")
    private TitleCategoryDAO titleCategoryDAO;

    public void create(TitleCategory titleCategory) throws CreateException {
        titleCategoryDAO.save(titleCategory);
    }

    public TitleCategory update(TitleCategory titleCategory) throws UpdateException {
        return titleCategoryDAO.save(titleCategory);
    }

    public TitleCategory delete(int id) throws DeleteException {
        TitleCategory titleCategory = titleCategoryDAO.findById(id);
        titleCategoryDAO.delete(titleCategory);
        return titleCategory;
    }

    public TitleCategory get(int id){
        return titleCategoryDAO.findById(id);
    }

    public List<TitleCategory> getAll(){
        return titleCategoryDAO.findAll();
    }

    public boolean exist(int id){
        return titleCategoryDAO.existsById(id);
    }
}

package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.MultimediaDAO;
import com.pablocontreras.proyectotesting.model.Multimedia;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_multimedia")
public class MultimediaDaoImpl {
    @Autowired
    @Qualifier("repositorio_multimedia")
    private MultimediaDAO multimediaDAO;

    public void create(Multimedia multimedia) throws CreateException {
        multimediaDAO.save(multimedia);
    }

    public Multimedia update(Multimedia multimedia) throws UpdateException {
        return multimediaDAO.save(multimedia);
    }

    public Multimedia delete(int id) throws DeleteException {
        Multimedia multimedia = multimediaDAO.findById(id);
        multimediaDAO.delete(multimedia);
        return multimedia;
    }

    public Multimedia get(int id){
        return multimediaDAO.findById(id);
    }

    public List<Multimedia> getAll(){
        return multimediaDAO.findAll();
    }

    public boolean exist(int id){
        return multimediaDAO.existsById(id);
    }
}

package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.LoanDAO;
import com.pablocontreras.proyectotesting.model.Loan;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_loan")
public class LoanDaoImpl {
    @Autowired
    @Qualifier("repositorio_loan")
    private LoanDAO loanDAO;

    public void create(Loan loan) throws CreateException {
        loanDAO.save(loan);
    }

    public Loan update(Loan loan) throws UpdateException {
        return loanDAO.save(loan);
    }

    public Loan delete(int id) throws DeleteException {
        Loan borrower = loanDAO.findByLoanid(id);
        loanDAO.delete(borrower);
        return borrower;
    }

    public Loan get(int id){
        return loanDAO.findByLoanid(id);
    }

    public List<Loan> getAll(){
        return loanDAO.findAll();
    }

    public boolean exist(int id){
        return loanDAO.existsByLoanid(id);
    }
}

package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.SuspensionRulesDAO;
import com.pablocontreras.proyectotesting.model.SuspensionRules;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_suspensionrules")
public class SuspensionRulesDaoImpl {
    @Autowired
    @Qualifier("repositorio_suspensionrules")
    private SuspensionRulesDAO suspensionRulesDAO;

    public void create(SuspensionRules suspensionRules) throws CreateException {
        suspensionRulesDAO.save(suspensionRules);
    }

    public SuspensionRules update(SuspensionRules suspensionRules) throws UpdateException {
        return suspensionRulesDAO.save(suspensionRules);
    }

    public SuspensionRules delete(int id) throws DeleteException {
        SuspensionRules suspensionRules= suspensionRulesDAO.findById(id);
        suspensionRulesDAO.delete(suspensionRules);
        return suspensionRules;
    }

    public SuspensionRules get(int id) {
        return suspensionRulesDAO.findById(id);
    }

    public List<SuspensionRules> getAll() {
        return suspensionRulesDAO.findAll();
    }

    public boolean exist(int id) {
        return suspensionRulesDAO.existsById(id);
    }
}

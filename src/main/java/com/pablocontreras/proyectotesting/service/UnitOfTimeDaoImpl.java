package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.UnitOfTimeDAO;
import com.pablocontreras.proyectotesting.model.UnitOfTime;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_unitoftime")
public class UnitOfTimeDaoImpl {
    @Autowired
    @Qualifier("repositorio_unitoftime")
    private UnitOfTimeDAO unitOfTimeDAO;

    public void create(UnitOfTime unitOfTime) throws CreateException {
        unitOfTimeDAO.save(unitOfTime);
    }

    public UnitOfTime update(UnitOfTime Suspension) throws UpdateException {
        return unitOfTimeDAO.save(Suspension);
    }

    public UnitOfTime delete(int id) throws DeleteException {
        UnitOfTime unitOfTime = unitOfTimeDAO.findByUtid(id);
        unitOfTimeDAO.delete(unitOfTime);
        return unitOfTime;
    }

    public UnitOfTime get(int id) {
        return unitOfTimeDAO.findByUtid(id);
    }

    public List<UnitOfTime> getAll() {
        return unitOfTimeDAO.findAll();
    }

    public boolean exist(int id) {
        return unitOfTimeDAO.existsByUtid(id);
    }
}

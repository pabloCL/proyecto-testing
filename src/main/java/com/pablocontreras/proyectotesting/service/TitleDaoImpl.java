package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.TitleDAO;
import com.pablocontreras.proyectotesting.model.Title;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_title")
public class TitleDaoImpl {
    @Autowired
    @Qualifier("repositorio_title")
    private TitleDAO titleDAO;
    public void create(Title title) throws CreateException {
        titleDAO.save(title);
    }

    public Title update(Title title) throws UpdateException {
        return titleDAO.save(title);
    }

    public Title delete(int id) throws DeleteException {
        Title title = titleDAO.findById(id);
        titleDAO.delete(title);
        return title;
    }

    public Title get(int id){
        return titleDAO.findById(id);
    }

    public List<Title> getAll(){
        return titleDAO.findAll();
    }

    public boolean exist(int id){
        return titleDAO.existsById(id);
    }
}

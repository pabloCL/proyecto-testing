package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.LoanConditionDAO;
import com.pablocontreras.proyectotesting.model.LoanCondition;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_loancondition")
public class LoanConditionDaoImpl {
    @Autowired
    @Qualifier("repositorio_loancondition")
    private LoanConditionDAO loanConditionDAO;
    public void create(LoanCondition loanCondition) throws CreateException {
        loanConditionDAO.save(loanCondition);
    }

    public LoanCondition update(LoanCondition loanCondition) throws UpdateException {
        return loanConditionDAO.save(loanCondition);
    }

    public LoanCondition delete(int id) throws DeleteException {
        LoanCondition loanCondition = loanConditionDAO.findById(id);
        loanConditionDAO.delete(loanCondition);
        return loanCondition;
    }

    public LoanCondition get(int id){
        return loanConditionDAO.findById(id);
    }

    public List<LoanCondition> getAll(){
        return loanConditionDAO.findAll();
    }

    public boolean exist(int id){
        return loanConditionDAO.existsById(id);
    }
}

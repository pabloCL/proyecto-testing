package com.pablocontreras.proyectotesting.service;

import com.pablocontreras.proyectotesting.dao.SuspensionDAO;
import com.pablocontreras.proyectotesting.model.Suspension;
import com.pablocontreras.proyectotesting.service.exceptions.CreateException;
import com.pablocontreras.proyectotesting.service.exceptions.DeleteException;
import com.pablocontreras.proyectotesting.service.exceptions.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicio_suspension")
public class SuspensionDaoImpl {
    @Autowired
    @Qualifier("repositorio_suspension")
    private SuspensionDAO suspensionDAO;

    public Suspension create(Suspension suspension) throws CreateException {
        suspensionDAO.save(suspension);
        return suspension;
    }

    public Suspension update(Suspension suspension) throws UpdateException {
        return suspensionDAO.save(suspension);
    }

    public Suspension delete(int id) throws DeleteException {
        Suspension suspension = suspensionDAO.findById(id);
        suspensionDAO.delete(suspension);
        return suspension;
    }

    public Suspension get(int id) {
        return suspensionDAO.findById(id);
    }

    public List<Suspension> getAll() {
        return suspensionDAO.findAll();
    }

    public boolean exist(int id) {
        return suspensionDAO.existsById(id);
    }
}

package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.Title;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("repositorio_title")
public interface TitleDAO extends JpaRepository<Title,Serializable> {

    public abstract Title findByName(String nombre);
    public abstract Title findById(int id);
    public abstract boolean existsById(String id);
}

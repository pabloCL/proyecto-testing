package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.Journal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("repositorio_journal")
public interface JournalDAO extends JpaRepository<Journal,Serializable> {
    public abstract Journal findById(int id);
    public abstract boolean existsById(int id);
}

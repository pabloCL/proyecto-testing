package com.pablocontreras.proyectotesting.dao;


import com.pablocontreras.proyectotesting.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("repositorio_subject")
public interface SubjectDAO extends JpaRepository<Subject,Serializable> {
    public abstract Subject findById(int id);
    public abstract boolean existsById(int id);
}

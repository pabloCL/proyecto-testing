package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.Suspension;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
@Repository("repositorio_suspension")
public interface SuspensionDAO extends JpaRepository<Suspension,Serializable> {
    public abstract Suspension findById(int id);
    public abstract boolean existsById(int id);
}

package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.Newspaper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("repositorio_newspaper")
public interface NewspaperDAO extends JpaRepository<Newspaper,Serializable> {
    public abstract Newspaper findById(int id);
    public abstract boolean existsById(int id);
}

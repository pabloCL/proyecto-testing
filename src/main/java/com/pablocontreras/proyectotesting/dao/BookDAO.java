package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("repositorio_book")
public interface BookDAO extends JpaRepository<Book,Serializable> {

    public abstract Book findByAutor(String nombre);
    public abstract Book findByIsbn(String isbn);
    public abstract boolean existsByIsbn(String isbn);
}

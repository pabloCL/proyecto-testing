package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.TitleCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("repositorio_titlecategory")
public interface TitleCategoryDAO extends JpaRepository<TitleCategory,Serializable> {
    public abstract TitleCategory findById(int id);
    public abstract boolean existsById(int id);
}

package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.BorrowerCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("repositorio_bcategory")
public interface BorrowerCategoryDAO extends JpaRepository<BorrowerCategory,Serializable> {

    public abstract BorrowerCategory findById(int id);
    public abstract boolean existsById(int id);
}

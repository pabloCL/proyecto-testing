package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.Multimedia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("repositorio_multimedia")
public interface MultimediaDAO extends JpaRepository<Multimedia,Serializable> {
    public abstract Multimedia findById(int id);
    public abstract boolean existsById(int id);
}

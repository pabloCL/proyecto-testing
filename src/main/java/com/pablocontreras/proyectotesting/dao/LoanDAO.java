package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.Loan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("repositorio_loan")
public interface LoanDAO  extends JpaRepository<Loan,Serializable> {
    public abstract Loan findByLoanid(int id);
    public abstract boolean existsByLoanid(int id);
}

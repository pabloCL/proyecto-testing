package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.Copy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("repositorio_copy")
public interface CopyDAO extends JpaRepository<Copy,Serializable> {
    public abstract Copy findById(int id);
    public abstract boolean existsById(int id);
}

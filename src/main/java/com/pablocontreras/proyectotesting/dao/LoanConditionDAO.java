package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.LoanCondition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("repositorio_loancondition")
public interface LoanConditionDAO extends JpaRepository<LoanCondition,Serializable> {
    public abstract LoanCondition findById(int id);
    public abstract boolean existsById(int id);
}

package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.Borrower;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.io.Serializable;
import java.util.List;

@Repository("repositorio_borrower")
public interface BorrowerDAO extends JpaRepository<Borrower,Serializable> {

    public abstract Borrower findByRut(int rut);
    public abstract boolean existsByRut(int rut);

    @Query(value = "select b.borrower_rut,b.borrower_name,b.borrower_email,b.borrower_cellphone, b2.borrowercategory_name from borrower b join borrowercategory b2 on b.borrowercategory_identifier = b2.borrowercategory_identifier",nativeQuery = true)
    public abstract List<Object[]> borrowersDetail();

}

package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.UnitOfTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.lang.invoke.SerializedLambda;
@Repository("repositorio_unitoftime")
public interface UnitOfTimeDAO extends JpaRepository<UnitOfTime,SerializedLambda> {
    public abstract UnitOfTime findByUtid(int id);
    public abstract boolean existsByUtid(int id);
}

package com.pablocontreras.proyectotesting.dao;

import com.pablocontreras.proyectotesting.model.SuspensionRules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("repositorio_suspensionrules")
public interface SuspensionRulesDAO extends JpaRepository<SuspensionRules,Serializable> {
    public abstract SuspensionRules findById(int id);
    public abstract boolean existsById(int id);
}

package com.pablocontreras.proyectotesting;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pablocontreras.proyectotesting.controller.SuspensionController;
import com.pablocontreras.proyectotesting.model.Suspension;
import com.pablocontreras.proyectotesting.service.SuspensionDaoImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(MockitoJUnitRunner.class)
public class SuspensionControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Mock
    private SuspensionDaoImpl suspensionDao;

    @InjectMocks
    private SuspensionController suspensionController;

    private ObjectMapper mapper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(suspensionController).build();
    }

    private List<Suspension> mockList(){
        //Creacion de objetos Suspension
        Suspension suspension1 = new Suspension();
        suspension1.setId(1);
        suspension1.setDescription("aaaaaaaaaaaa");
        suspension1.setNumberofunitoftime(5);
        suspension1.setUnitoftime(3);
        suspension1.setRut(112390693);

        Suspension suspension2 = new Suspension();
        suspension2.setId(2);
        suspension2.setDescription("bbbbbbbbbbbb");
        suspension2.setNumberofunitoftime(3);
        suspension2.setUnitoftime(2);
        suspension2.setRut(174592225);

        //Lista de suspensiones
        List<Suspension> mockList = new ArrayList<>();
        //Se añaden los objetos a la lista
        mockList.add(suspension1);
        mockList.add(suspension2);
        return mockList;
    }

    private Suspension singleSuspension(){
        //Creacion de objetos Suspension
        Suspension suspension = new Suspension();
        suspension.setId(1);
        suspension.setDescription("aaaaaaaaaaaa");
        suspension.setNumberofunitoftime(5);
        suspension.setUnitoftime(3);
        suspension.setRut(112390693);
        return suspension;
    }

    @Test
    public void createsuspension() throws Exception {
        Suspension suspension = singleSuspension();

        when(suspensionDao.create(Mockito.any(Suspension.class))).thenReturn(suspension);

        Suspension creado = suspensionDao.create(suspension);

        Assert.assertNotNull("Failure: Expected not null", creado);
        Assert.assertNotNull("Failure: Expected id not null", creado.getId());
        Assert.assertEquals("Failure: Expected description attribute match", "aaaaaaaaaaaa", creado.getDescription());

        verify(suspensionDao, times(1)).create(suspension);

        String input = mapper.writeValueAsString(suspension);

        String URI = "/biblioteca/suspension";

        mockMvc.perform(get(URI)).andExpect(status().isOk());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.put(URI)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(input);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        int status = response.getStatus();

        Assert.assertEquals(200, status);
    }

    @Test
    public void findAllSuspensionsTest() throws Exception {
        List<Suspension> mockList = mockList();
        //Se condiciona mediante mocks
        when(suspensionDao.getAll()).thenReturn(mockList);

        List<Suspension> suspensiones = suspensionDao.getAll();
        //Se valida si la lista es nula, el tamaño de la lista y comparacion de atributos
        Assert.assertNotNull("Expected: not null", suspensiones);
        Assert.assertEquals("Failure: Expected size", 2, suspensiones.size());
        Assert.assertEquals("Expected: Description attribute Match", "bbbbbbbbbbbb", suspensiones.get(1).getDescription());
        //verificar cantidad de veces que es llamado le método
        verify(suspensionDao, times(1)).getAll();

        //Url del web service
        String URI = "/biblioteca/suspension";

        //Creacion de la petición
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON_UTF8);
        //Se ejecuta la petición
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        //Se crea un mock de la respuesta
        MockHttpServletResponse response = mvcResult.getResponse();
        //se almacena status de la respuesta
        int status = response.getStatus();
        //Lista esperada convertida a String mediante ObjectMapper
        String expected = mapper.writeValueAsString(mockList);
        //String con resultado de la consulta
        String result = mvcResult.getResponse().getContentAsString();
        //Se comprueba que el status sea 200 y el resultado sea igual al esperado
        Assert.assertEquals(200, status);
        assertThat(expected).isEqualTo(result);
    }

    @Test
    public void findSuspensionbyID() throws Exception {
        Suspension suspension = singleSuspension();

        when(suspensionDao.get(Mockito.any(Integer.class))).thenReturn(suspension);

        Suspension resultado = suspensionDao.get(1);
        Assert.assertNotNull("Expected: not null", resultado);
        Assert.assertEquals("Failure: Expected id attribute match", 1, resultado.getId());

        verify(suspensionDao, times(1)).get(1);

        String URI = "/biblioteca/suspension/1";

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        int status = response.getStatus();

        Assert.assertEquals(200, status);

    }

    @Test(expected = NullPointerException.class)
    public void findSuspensionbyIDThrowsException() throws NullPointerException {
        Suspension suspension = singleSuspension();

        when(suspensionDao.get(1)).thenReturn(suspension);
        Suspension resultado = suspensionDao.get(2);
        Assert.assertNull("Expected: null", resultado);
        Assert.assertEquals(1, resultado.getId());

    }

    @Test
    public void SuspensionNotFound() throws Exception {
        Integer id = Integer.MAX_VALUE;

        Suspension resultado = suspensionDao.get(id);

        Assert.assertNull("Expected: null", resultado);

    }

    @Test
    public void SuspensionUpdateTest() throws Exception {
        Suspension suspension = singleSuspension();

        when(suspensionDao.get(Mockito.any(Integer.class))).thenReturn(suspension);

        Suspension resultado = suspensionDao.get(1);

        Assert.assertNotNull("Expected: not null", resultado);

        String updateDescription = resultado.getDescription() + " test";
        resultado.setDescription(updateDescription);
        when(suspensionDao.update(Mockito.any(Suspension.class))).thenReturn(resultado);
        Suspension updatedSuspension = suspensionDao.update(resultado);

        Assert.assertNotNull("Expected: not null", updatedSuspension);
        Assert.assertEquals("Expected: Description attribute Match", "aaaaaaaaaaaa test", updatedSuspension.getDescription());
    }

    @Test
    public void DeleteSuspensionTest() throws Exception {

        //Lista de suspensiones
        List<Suspension> mockList = mockList();

        when(suspensionDao.get(1)).thenReturn(mockList.get(0));
        Suspension resultado = suspensionDao.get(1);

        Assert.assertNotNull("Expected: not null", resultado);
        verify(suspensionDao, times(1)).get(Mockito.any(Integer.class));

        when(suspensionDao.delete(1)).thenReturn(mockList.get(0));

        Suspension eliminado = suspensionDao.delete(1);

        Assert.assertNotNull("Expected: not null", eliminado);
        Assert.assertEquals("Expected id attribute match",1,eliminado.getId());
        verify(suspensionDao, times(1)).delete(Mockito.any(Integer.class));

        mockList.remove(0);

        when(suspensionDao.getAll()).thenReturn(mockList);

        List<Suspension> finalList = suspensionDao.getAll();

        Assert.assertEquals("Failure: Expected size ",1,finalList.size());

        verify(suspensionDao, times(1)).getAll();

        //Test Controller
        String URI = "/biblioteca/suspension/1";

        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(URI).contentType(MediaType.APPLICATION_JSON_UTF8);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        int status = response.getStatus();

        Assert.assertEquals(200,status);


    }
}

package com.pablocontreras.proyectotesting;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pablocontreras.proyectotesting.controller.BorrowerController;
import com.pablocontreras.proyectotesting.model.BCategoryBorrower;
import com.pablocontreras.proyectotesting.model.Borrower;
import com.pablocontreras.proyectotesting.service.BorrowerDaoImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableWebMvc
@AutoConfigureMockMvc
public class BorrowerControllerTest {

    private TestRestTemplate testRestTemplate = new TestRestTemplate();
    private HttpHeaders headers = new HttpHeaders();

    @Autowired
    MockMvc mockMvc;

    @Mock
    private BorrowerDaoImpl borrowerDao;

    @InjectMocks
    private BorrowerController borrowerController;

    @LocalServerPort
    private static final int port = 8080;

    private ObjectMapper mapper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(borrowerController).build();
    }

    private List<Borrower> borrowerList() {
        Borrower mock1 = new Borrower();
        mock1.setRut(112390693);
        mock1.setBorrower_name("Lorena Contreras");
        mock1.setCellphone("972713796");
        mock1.setEmail("lorena@gmail.com");

        Borrower mock2 = new Borrower();
        mock2.setRut(174592252);
        mock2.setBorrower_name("Pablo Bustos");
        mock2.setCellphone("654789321");
        mock2.setEmail("pablo@gmail.com");

        List<Borrower> mockList = new ArrayList<>();

        mockList.add(mock1);
        mockList.add(mock2);
        return mockList;
    }

    private Borrower mockBorrower1() {
        //Creacion de objetos Suspension
        Borrower mock1 = new Borrower();
        mock1.setRut(174592225);
        mock1.setBorrower_name("Pablo Contreras");
        mock1.setCellphone("654987321");
        mock1.setEmail("pablo@gmail.com");
        mock1.setBcid(1);
        return mock1;
    }
    private Borrower mockBorrower2() {
        //Creacion de objetos Suspension
        Borrower mock2 = new Borrower();
        mock2.setRut(987654321);
        mock2.setBorrower_name("Test Test");
        mock2.setCellphone("547896321");
        mock2.setEmail("test@gmail.com");
        mock2.setBcid(1);
        return mock2;
    }

    @Test
    public void createBorrower() throws Exception {
        //todo: Arrange
        //Se define la URI que se desea probar
        String URI = "/biblioteca/borrower";
        String URI2 = "/biblioteca/borrower/987654321";
        //Se crea mock Borrower
        Borrower mockBorrower = mockBorrower2();
        //Se configura lo que debe retornar al realizar la petición
        when(borrowerDao.create(mockBorrower)).thenReturn(mockBorrower);
        //todo: Test Servicio
        //todo: Act 1
        //Se ejecuta la prueba del servicio que es un objeto de tipo Borrower
        Borrower creado = borrowerDao.create(mockBorrower);
        //TODO: Assert 1
        Assert.assertNotNull("Failure: Expected not null", creado);
        Assert.assertEquals("Failure: Expected name attribute match", "Test Test", creado.getBorrower_name());
        //Se verifica el numero de invocaciones del método
        verify(borrowerDao, times(1)).create(mockBorrower);
        //todo: Test MockMVC
        //Todo: Act 2
        //Se convierte el objeto a json para la petición
        String input = mapper.writeValueAsString(mockBorrower);
        //Prepara la petición
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(createURLWithPort(URI))
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(input);

        //Ejecuta la petición
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        //Se crea un mock de la respuesta
        MockHttpServletResponse response = mvcResult.getResponse();
        //Se crea un mock de la respuesta
        Borrower mvcBorrower = mapper.readValue(response.getContentAsString(), Borrower.class);
        //TODO: Assert 2
        //Se verifica que la petición haya sido exitosa, que el objeto no sea nulo y se comparan parametros entre esperado y resultado
        Assert.assertEquals(200, response.getStatus());
        Assert.assertNotNull("Expected: not null",mvcBorrower);
        Assert.assertEquals(mockBorrower.getBorrower_name(),mvcBorrower.getBorrower_name());
        //todo: Test a servidor local
        //Todo: Act 3: se creará un borrower de prueba que será insertado en la base de datos
        //se crea entidad con tipo borrower con los datos a añadir a la petición
        HttpEntity<Borrower> borrowerHttpEntity = new HttpEntity<Borrower>(mockBorrower, headers);
        //se ejecuta la petición para crear un Borrower
        ResponseEntity<String> responseCreate = testRestTemplate.exchange(
                createURLWithPort(URI),
                HttpMethod.POST, borrowerHttpEntity, String.class);
        //se obtiene el status code de la petición
        HttpStatus statusCreate  = responseCreate.getStatusCode();
        //Se verifica el tamaño de la nueva lista con el esperado insertado el borrower de prueba
        Assert.assertEquals(4,getBorrowersAPI().size());
        //TODO: Assert 3
        //Se verifica el codigo de la petición con el esperado
        Assert.assertEquals(HttpStatus.OK,statusCreate);
        //Todo: Se elimina el borrower de prueba
        //Se prepara la entidad con el tipo del contenido en este caso la petición recibe el rut del borrower
        HttpEntity<Integer> deleteHttpEntity = new HttpEntity<Integer>(987654321, headers);
        //se ejecuta la petición para eliminar el Borrower de prueba
        ResponseEntity<String> stringResponseEntity = testRestTemplate.exchange(
                createURLWithPort(URI2),
                HttpMethod.DELETE, deleteHttpEntity, String.class);
        //se obtiene el status code de la petición
        HttpStatus httpStatus  = stringResponseEntity.getStatusCode();
        //Se verifica el codigo de la petición con el esperado
        Assert.assertEquals(HttpStatus.OK,httpStatus);
        //Se obtiene la lista y el tamaño una vez eliminado el borrower de prueba
        Assert.assertEquals(3,getBorrowersAPI().size());
        //Se verifica el numero de invocaciones del método crear
        verify(borrowerDao, times(1)).create(mockBorrower);

    }

    @Test
    public void updateBorrower() throws Exception {
        //todo: Arrange
        //Se define la URI que se desea probar
        String URI = "/biblioteca/borrower";
        String updateName = "Felipe Contreras";
        //Se crea mock Borrower
        Borrower mockBorrower = mockBorrower1();
        //Se define el mock del borrower actualizado
        Borrower mockUpdated = mockBorrower;
        //Se modifica el valor borrower_name del mock actualizado
        mockUpdated.setBorrower_name(updateName);
        //Se configura lo que debe retornar al realizar la petición
        when(borrowerDao.update(mockBorrower)).thenReturn(mockUpdated);
        //Se convierte el objeto a Json
        String input = mapper.writeValueAsString(mockUpdated);
        //todo: Act 1
        Borrower updatedBorrower = borrowerDao.update(mockBorrower);
        //TODO: Assert 1
        //se verifica que no sea nulo
        Assert.assertNotNull("Expected: not null", updateName);
        //Se comparan los datos esperados con el resultado
        Assert.assertEquals("Expected: Description attribute Match", "Felipe Contreras", updatedBorrower.getBorrower_name());
        //todo: Act 2
        RequestBuilder requestBuilder = MockMvcRequestBuilders.put(createURLWithPort(URI))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(input);
        //Ejecuta la petición
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        //Se crea un mock de la respuesta
        MockHttpServletResponse mvcResultResponse = mvcResult.getResponse();
        //Todo: Assert 2
        //Se verifica que la petición haya sido exitosa
        Assert.assertEquals(200, mvcResultResponse.getStatus());
        //Se convierte el json de la respuesta a un objeto de tipo Borrower
        Borrower mvcBorrower = mapper.readValue(mvcResultResponse.getContentAsString(), Borrower.class);
        //Se verifica que no sea nulo
        assertNotNull(mvcBorrower);
        //Se verifica que los valores sean iguales
        Assert.assertEquals(mockUpdated.getBorrower_name(),mvcBorrower.getBorrower_name());
        Assert.assertEquals(mockUpdated.getRut(),mvcBorrower.getRut());
        Assert.assertEquals(mockUpdated.getEmail(),mvcBorrower.getEmail());
        Assert.assertEquals(mockUpdated.getCellphone(),mvcBorrower.getCellphone());
        //Se verifica el numero de invocaciones del método
        verify(borrowerDao, times(1)).update(mockBorrower);
        //todo: Test a servidor local
        //Todo: Act 3: se modificará un borrower de prueba en la base de datos
        //se crea entidad con tipo borrower con los datos a añadir a la petición
        HttpEntity<Borrower> updateHttpEntity = new HttpEntity<Borrower>(mockUpdated, headers);
        //se ejecuta la petición para crear un Borrower
        ResponseEntity<Borrower> responseCreate = testRestTemplate.exchange(
                createURLWithPort(URI),
                HttpMethod.PUT, updateHttpEntity, Borrower.class);
        //se obtiene el status code de la petición
        HttpStatus statusUpdate  = responseCreate.getStatusCode();
        //Se verifica que el status code sea 200 ó OK
        Assert.assertEquals(HttpStatus.OK,statusUpdate);
        //Se crea un objeto de tipo borrower con la respuesta de la petición
        Borrower httpBorrower = responseCreate.getBody();
        //Se verifica que no sea nulo
        assertNotNull(httpBorrower);
        //Se verifica que los valores sean iguales
        Assert.assertEquals(mockUpdated.getBorrower_name(),httpBorrower.getBorrower_name());
        //todo: se vuelve a dejar el objeto con sus datos originales
        //se crea entidad con tipo borrower con los datos a añadir a la petición
        HttpEntity<Borrower> borrowerHttpEntity = new HttpEntity<Borrower>(mockBorrower1(), headers);
        //se ejecuta la petición para crear un Borrower
        ResponseEntity<Borrower> responseEntity = testRestTemplate.exchange(
                createURLWithPort(URI),
                HttpMethod.PUT, borrowerHttpEntity, Borrower.class);
    }

    @Test
    public void deleteBorrower() throws Exception {
        //todo: Arrange
        //Se define la URI que se desea probar
        String URI = "/biblioteca/borrower/174592225";
        String createURI = "/biblioteca/borrower";
        //Se crea un mock de tipo Borrower
        Borrower expected = mockBorrower1();
        //Se configura lo que debe retornar al realizar la petición
        when(borrowerDao.delete(174592225)).thenReturn(expected);
        //todo: Test Servicio
        //todo: Act 1
        //Se ejecuta el servicio delete
        Borrower eliminado = borrowerDao.delete(174592225);
        //TODO: Assert 1
        //Assert si es nulo y si el rut del eliminado es el correcto
        Assert.assertNotNull("Expected: not null", eliminado);
        Assert.assertEquals("Expected rut attribute match", expected.getRut(), eliminado.getRut());
        //todo: Test MockMVC
        //Todo: Act 2
        //Prepara la petición
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(createURLWithPort(URI))
                .contentType(MediaType.APPLICATION_JSON_UTF8);
        //Ejecuta la petición
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        //Se crea un mock de la respuesta
        MockHttpServletResponse mvcResultResponse = mvcResult.getResponse();
        //Se almacena el status en una variable de tipo int
        int status = mvcResultResponse.getStatus();
        //TODO: Assert 2
        //Assert status code
        Assert.assertEquals(200, status);
        //todo: Test a servidor local
        //Todo: Act 3: se eliminara un borrower de prueba en la base de datos
        //se crea entidad con tipo borrower con los datos a añadir a la petición
        HttpEntity<Borrower> deleteHttpEntity = new HttpEntity<Borrower>(expected, headers);
        //se ejecuta la petición para crear un Borrower
        ResponseEntity<Borrower> responseCreate = testRestTemplate.exchange(
                createURLWithPort(URI),
                HttpMethod.DELETE, deleteHttpEntity, Borrower.class);
        //se obtiene el status code de la petición
        HttpStatus statusUpdate  = responseCreate.getStatusCode();
        //Se verifica que el status code sea 200 ó OK
        Assert.assertEquals(HttpStatus.OK,statusUpdate);
        //todo: se vuelve a insertar el borrower eliminado para dejar la bdd en su estado original
        //se crea entidad con tipo borrower con los datos a añadir a la petición
        HttpEntity<Borrower> borrowerHttpEntity = new HttpEntity<Borrower>(expected, headers);
        //se ejecuta la petición para crear un Borrower
        ResponseEntity<Borrower> responseEntity = testRestTemplate.exchange(
                createURLWithPort(createURI),
                HttpMethod.POST, borrowerHttpEntity, Borrower.class);
        Assert.assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
    }

    @Test
    public void getBorrower() throws Exception {
        //todo: Arrange
        //Se define la URI que se desea probar
        String URI = "/biblioteca/borrower/174592225";
        //Se crea un mock de tipo Borrower
        Borrower mockBorrower = mockBorrower1();
        //Se configura lo que debe retornar al realizar la petición
        when(borrowerDao.get(174592225)).thenReturn(mockBorrower);
        //todo: Test Servicio
        //todo: Act 1
        //Se ejecuta la prueba del servicio que es un objeto de tipo Borrower
        Borrower resultado = borrowerDao.get(174592225);
        //TODO: Assert 1
        Assert.assertNotNull("Expected: not null", resultado);
        Assert.assertEquals("Failure: Expected rut attribute match", 174592225, resultado.getRut());
        //todo: Test MockMVC
        //Todo: Act 2
        //Prepara la petición
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(createURLWithPort(URI))
                .contentType(MediaType.APPLICATION_JSON_UTF8);
        //Ejecuta la petición
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        //Se crea un mock de la respuesta
        MockHttpServletResponse mvcResultResponse = mvcResult.getResponse();
        //Se convierte la respuesta a al objeto borrower mediante objectmapper
        Borrower mvcBorrower = mapper.readValue(mvcResult.getResponse().getContentAsString(), Borrower.class);
        //Se almacena el status en una variable de tipo int
        int status = mvcResultResponse.getStatus();
        //TODO: Assert 2
        //Assert status code
        Assert.assertEquals(200, status);
        //Se verifica que no sea nulo
        assertNotNull(mvcBorrower);
        //Se verifica que los valores sean iguales
        Assert.assertEquals(mockBorrower.getBorrower_name(),mvcBorrower.getBorrower_name());
        Assert.assertEquals(mockBorrower.getRut(),mvcBorrower.getRut());
        Assert.assertEquals(mockBorrower.getEmail(),mvcBorrower.getEmail());
        Assert.assertEquals(mockBorrower.getCellphone(),mvcBorrower.getCellphone());
        //verificar cantidad de veces que es llamado le método
        verify(borrowerDao, times(2)).get(174592225);
        //todo: Test a servidor local
        //Todo: Act 3
        //Se realiza la petición que debe retornar una list de tipo Borrower
        ResponseEntity<Borrower> responseEntity = testRestTemplate.exchange(
                createURLWithPort(URI),
                HttpMethod.GET, null, new ParameterizedTypeReference<Borrower>() {
                });
        //Todo: Assert 3
        //Se prueba si la petición es exitosa
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        //Si es exitoso almacena la lista con los datos de la base de datos
        Borrower httpBorrower = responseEntity.getBody();
        //Assert para comparar si es nulo el objeto y match de atributos
        Assert.assertNotNull("Expected: not null", httpBorrower);
        Assert.assertEquals("Expected: name attribute Match", mockBorrower.getBorrower_name(), httpBorrower.getBorrower_name());
        Assert.assertEquals("Expected: rut attribute Match", mockBorrower.getRut(), httpBorrower.getRut());
        Assert.assertEquals("Expected: email attribute Match", mockBorrower.getEmail(), httpBorrower.getEmail());
        Assert.assertEquals("Expected: cellphone attribute Match", mockBorrower.getCellphone(), httpBorrower.getCellphone());
    }

    @Test
    public void getAllBorrowers() throws Exception {
        //todo: Arrange
        //Se define la URI que se desea probar
        String URI = "/biblioteca/borrower";
        //Se crea un mock de la lista tipo Borrower
        List<Borrower> mockList = borrowerList();
        //Se configura lo que debe retornar al realizar la petición
        when(borrowerDao.getAll()).thenReturn(mockList);
        //todo: Test Servicio
        //todo: Act 1
        List<Borrower> borrowers = borrowerDao.getAll();
        //TODO: Assert 1
        //Se valida si la lista es nula, el tamaño de la lista y comparacion de atributos
        Assert.assertNotNull("Expected: not null", borrowers);
        Assert.assertEquals("Failure: Expected size", 2, borrowers.size());
        Assert.assertEquals("Expected: email attribute Match", "pablo@gmail.com", borrowers.get(1).getEmail());
        //todo: Test MockMVC
        //Todo: Act 2
        //Prepara la petición
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(createURLWithPort(URI))
                .contentType(MediaType.APPLICATION_JSON_UTF8);
        //Ejecuta la petición
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        //Se crea un mock de la respuesta
        MockHttpServletResponse mvcResultResponse = mvcResult.getResponse();
        //Se almacena el status en una variable de tipo int
        int status = mvcResultResponse.getStatus();
        //TODO: Assert 2
        //Assert status code
        Assert.assertEquals(200, status);
        //Se almacena el contenido de la respuesta
        String result = mvcResult.getResponse().getContentAsString();
        //Valor esperado de objeto a String
        String expected = mapper.writeValueAsString(mockList);
        //Comparación del resultado esperado con el resultado
        Assert.assertEquals(expected, result);
        //verificar cantidad de veces que es llamado le método
        verify(borrowerDao, times(2)).getAll();
        //todo: Test a servidor local
        //Todo: Act 3
        //Se realiza la petición que debe retornar una list de tipo BCategoryBorrower
        ResponseEntity<List<Borrower>> responseEntity = testRestTemplate.exchange(
                createURLWithPort(URI),
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Borrower>>() {
                });
        //Todo: Assert 3
        //Se prueba si la petición es exitosa
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        //Si es exitoso almacena la lista con los datos de la base de datos
        List<Borrower> borrowerList = responseEntity.getBody();
        //Assert para comparar si es nula la lista, el tamaño y match de atributos
        Assert.assertNotNull("Expected: not null", borrowerList);
        Assert.assertEquals("Expected size", 3, borrowerList.size());
        Assert.assertEquals("Expected: name attribute Match", "Pablo Contreras",
                borrowerList.get(1).getBorrower_name());
    }

    @Test
    public void CheckIfUserExistByRut() throws Exception {
        //todo: Arrange
        //Se define la URI que se desea probar
        String URI = "/biblioteca/borrower/exists/112390693";
        //Se configura lo que debe retornar al realizar la petición
        when(borrowerDao.exist(112390693)).thenReturn(true);
        //todo:Test servicio
        //todo: Act 1
        boolean existe = borrowerDao.exist(112390693);
        //todo: Assert 1
        assertTrue(existe);
        //TODO: Test MockMvc
        //todo: Act 2
        //Prepara la petición
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(createURLWithPort(URI)).contentType(MediaType.APPLICATION_JSON_UTF8);
        //Ejecuta la petición
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        //Se crea un mock de la respuesta
        MockHttpServletResponse mvcResultResponse = mvcResult.getResponse();
        //Se almacena el status en una variable de tipo int
        int status = mvcResultResponse.getStatus();
        //TODO: Assert 2
        //Assert status code
        Assert.assertEquals(200, status);
        //Se almacena el contenido de la respuesta
        String result = mvcResult.getResponse().getContentAsString();
        //Se verifica que el resultado sea igual al esperado
        assertThat("true").isEqualTo(result);
        //Se verifica el numero de invocaciones
        verify(borrowerDao, times(2)).exist(Mockito.anyInt());
        //todo: Test a servidor local
        //Todo: Act 3
        //Se realiza la petición que debe retornar el valor de tipo boolean
        ResponseEntity<Boolean> responseEntity = testRestTemplate.exchange(
                createURLWithPort(URI),
                HttpMethod.GET, null, new ParameterizedTypeReference<Boolean>() {
                });
        //Todo: Assert 3
        //Se prueba si la petición es exitosa
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        //Si es exitoso almacena la lista con los datos de la base de datos
        boolean responseEntityBody = responseEntity.getBody();
        //Assert para comparar si es nula la respuesta y si es verdadero
        Assert.assertNotNull("Expected: not null", responseEntityBody);
        assertThat(true).isEqualTo(responseEntityBody);
    }

    @Test
    public void NumberOfBorrowersIsTwo() throws Exception {
        //todo: Arrange
        //Se crea un mock de la lista tipo Borrower
        List<Borrower> mockList = borrowerList();
        //Se configura lo que debe retornar al realizar la petición
        when(borrowerDao.count()).thenReturn((long) mockList.size());
        //Se define la URI que se desea probar
        String URI = "/biblioteca/borrower/count";
        //todo: Test Servicio
        //todo: Act 1
        long count = borrowerDao.count();
        //todo: Assert 1
        //Se comprueba que el valor de count no sea nulo
        Assert.assertNotNull("Expected: not null", count);
        //Se comprueba que el valor de count sea el esperado
        Assert.assertEquals("Expected: count size", 2L, count);
        //TODO: Test MockMvc
        //todo: Act 2
        //Prepara la petición
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(createURLWithPort(URI))
                .contentType(MediaType.APPLICATION_JSON_UTF8);
        //Ejecuta la petición
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        //Se crea un mock de la respuesta
        MockHttpServletResponse mvcResultResponse = mvcResult.getResponse();
        //Se almacena el status en una variable de tipo int
        int status = mvcResultResponse.getStatus();
        //TODO: Assert 2
        //Assert status code
        Assert.assertEquals(200, status);
        //Se almacena el contenido de la respuesta
        String result = mvcResult.getResponse().getContentAsString();
        assertThat("2").isEqualTo(result);
        //se verifica el numero de invocaciones del servicio
        verify(borrowerDao, times(2)).count();
        //todo: Test a servidor local
        //Todo: Act 3
        //Se realiza la petición que debe retornar el valor de tipo long
        ResponseEntity<Long> responseEntity = testRestTemplate.exchange(
                createURLWithPort(URI),
                HttpMethod.GET, null, new ParameterizedTypeReference<Long>() {
                });
        //Todo: Assert 3
        //Se prueba si la petición es exitosa
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        //Si es exitoso almacena la lista con los datos de la base de datos
        long responseEntityBody = responseEntity.getBody();
        //Assert para comparar si es nula la lista, el tamaño
        Assert.assertNotNull("Expected: not null", responseEntityBody);
        Assert.assertEquals("Expected size", 3, responseEntityBody);
    }

    @Test
    public void getBorrowersCategoryTest() throws Exception {
        //todo: arrange
        //se crea un mock del tipo borrower
        Borrower borrowerMock = mockBorrower1();
        //se crea un mock del tipo BCategoryBorrower
        BCategoryBorrower catBorrowerMock = new BCategoryBorrower();
        //Se setean sus variables
        catBorrowerMock.setName(borrowerMock.getBorrower_name());
        catBorrowerMock.setRut(borrowerMock.getRut());
        catBorrowerMock.setEmail(borrowerMock.getEmail());
        catBorrowerMock.setPhone(borrowerMock.getCellphone());
        catBorrowerMock.setCategoria("funcionarios");
        //Se crea un mock de la lista tipo BCategoryBorrower
        List<BCategoryBorrower> mockList = new ArrayList<>();
        //Se añade el objeto creado
        mockList.add(catBorrowerMock);
        //Se define la URI que se desea probar
        String URI = "/biblioteca/borrower/detail";
        //Se configura lo que debe retornar al realizar la petición
        when(borrowerDao.borrowersDetail()).thenReturn(mockList);
        //todo: Test Servicio
        //todo: Act 1
        //Se ejecuta la prueba del servicio que es una lista de tipo BCategoryBorrower
        List<BCategoryBorrower> resultado = borrowerDao.borrowersDetail();
        //TODO: Assert 1
        //Se valida si la lista es nula, el tamaño de la lista y comparacion de atributos
        Assert.assertNotNull("Expected: not null", resultado);
        Assert.assertEquals("Failure: Expected size", 1, resultado.size());
        Assert.assertEquals("Expected: email attribute Match", "pablo@gmail.com",
                resultado.get(0).getEmail());
        //todo: Test MockMVC
        //Todo: Act 2
        //Prepara la petición
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(createURLWithPort(URI))
                .contentType(MediaType.APPLICATION_JSON_UTF8);
        //Ejecuta la petición
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        //Se crea un mock de la respuesta
        MockHttpServletResponse mvcResultResponse = mvcResult.getResponse();
        //Se almacena el status en una variable de tipo int
        int status = mvcResultResponse.getStatus();
        //TODO: Assert 2
        //Assert status code
        Assert.assertEquals(200, status);
        //Se almacena el contenido de la respuesta
        String result = mvcResult.getResponse().getContentAsString();
        //Valor esperado de objeto a String
        String expected = mapper.writeValueAsString(mockList);
        //Comparación del resultado esperado con el resultado
        Assert.assertEquals(expected, result);
        //Se verifica el numero de invocaciones del servicio
        verify(borrowerDao, times(2)).borrowersDetail();
        //todo: Test a servidor local
        //Todo: Act 3
        //Se realiza la petición que debe retornar una list de tipo BCategoryBorrower
        ResponseEntity<List<BCategoryBorrower>> responseEntity = testRestTemplate.exchange(
                createURLWithPort(URI),
                HttpMethod.GET, null, new ParameterizedTypeReference<List<BCategoryBorrower>>() {
                });
        //Todo: Assert 3
        //Se prueba si la petición es exitosa
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        //Si es exitoso almacena la lista con los datos de la base de datos
        List<BCategoryBorrower> bCategoryBorrowerList = responseEntity.getBody();
        //Assert para comparar si es nula la lista, el tamaño y match de atributos
        Assert.assertNotNull("Expected: not null", bCategoryBorrowerList);
        Assert.assertEquals("Expected size", 3, bCategoryBorrowerList.size());
        Assert.assertEquals("Expected: name attribute Match", "Pablo Contreras",
                bCategoryBorrowerList.get(1).getName());
        Assert.assertEquals("Expected: category attribute Match", "funcionarios",
                bCategoryBorrowerList.get(1).getCategoria());
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    private List<Borrower> getBorrowersAPI(){
        String URI = "/biblioteca/borrower";
        ResponseEntity<List<Borrower>> responseEntity = testRestTemplate.exchange(
                createURLWithPort(URI),
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Borrower>>() {
                });

        return responseEntity.getBody();
    }
}

package com.pablocontreras.proyectotesting;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pablocontreras.proyectotesting.controller.LoanController;
import com.pablocontreras.proyectotesting.service.LoanDaoImpl;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class LoanControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Mock
    private LoanDaoImpl loanDao;

    @InjectMocks
    private LoanController loanController;

    private ObjectMapper mapper;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        mapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(loanController).build();
    }
}
